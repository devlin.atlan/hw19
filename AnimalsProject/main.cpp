#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Animal
{
protected:
	//�� ������ ������ ������������� ���������� 
	string AnimalSpeech ="Hola, human."; 
public:
	//��������� �����������. ����� ����������
	Animal() {}
	//����������� � ������� ������� ���������� ����� �����
	Animal(string _asp) : AnimalSpeech(_asp) {}
	//��������� ����������. ������� ������ �� AnimalSpeech
	virtual ~Animal() {   }

	virtual void Voice()
	{
		cout << "It's animal voice: " << AnimalSpeech << "\n";
	}
};

//����� ���������������� ������ ��������, ���������� �� ������������� 
//������� ���������� �������, � ����������� ������� ������ ���� ����

class Cow : public Animal
{
public: 
	Cow(string _csp = "Moo.") : Animal(_csp) {}
	void Voice() override
	{
		cout << "Cow says: "<< AnimalSpeech << "\n";
	}
};

class Frog : public Animal
{
public:
	Frog(string _csp = "Ribbit.") : Animal(_csp) {}
	void Voice() override
	{
		cout << "Frog says: " << AnimalSpeech << "\n";
	}
};

class Dinosaur : public Animal
{
public:
	Dinosaur(string _csp = "RRRR!") : Animal(_csp) {}
	void Voice() override
	{
		cout << "Dinosaur says: " << AnimalSpeech << "\n";
	}
};
int main()
{
	// ����� ������ ������� � ����������-������� ��� �������� ��� ����������
	int ArrSize = 9, i = 0;
	//std::vector<Animal*> arr(ArrSize);

	//������ ��������� �� ������, ���������� �������� �� Animal ������
	Animal *arr = new Animal[10];
	//�������� ������ ������ ���������
	arr[i] = Cow(); i++;
	arr[i] = Cow("Moo?"); i++;
	arr[i] = Cow("MOOOOO!!!!"); i++;
	arr[i] = Frog(); i++;
	arr[i] = Frog("Croak?"); i++;
	arr[i] = Frog("QUAAA!"); i++;
	arr[i] = Dinosaur(); i++;
	arr[i] = Dinosaur("Damn, I like Cretaceous!"); i++;
	arr[i] = Dinosaur("What a beautiful meteor... Oh, wait...");

	//� ��������� �� ����������
	for (int j=0; j< ArrSize; j++) 
	{
		arr[j].Voice();
	}

	//������ �� ������ ������, ���������� �������� �� Animal ������
	delete[] arr;

	//����� ��������
	system("pause");
	return 0;
}
